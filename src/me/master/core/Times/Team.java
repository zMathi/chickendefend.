package me.master.core.Times;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface Team {

	public String getNome();
	public Location getSpawn();
	public ArrayList<Player> getPlayers();
	public void giveItem();
	public void giveItem(Player p);
	public void addPlayer();
	public void removePlayer();
	public void tpToSpawn();
	public void tpToSpawn(Player p);
}
