package me.master.core;

public enum Estagio {
	 FECHADO,ENTRAR,INGAME,RESETANDO;
	 	
	 @Override
	 public String toString(){
		 switch(this){
		 case FECHADO : return "Fechado";
		 case ENTRAR : return "Entrar";
		 case INGAME : return "Ingame";
		 case RESETANDO : return "Resetando";
		 default : throw new IllegalArgumentException();
		 }
	 }

}
